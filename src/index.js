import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'


import Routes from './routes';

import './index.css';

const AppWithRouter = () => (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  )
  
ReactDOM.render(
    <AppWithRouter/> ,
  document.getElementById('root')
);