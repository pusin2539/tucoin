
import firebase from "firebase";
var config = {
    apiKey: "AIzaSyD7BvGPy9WTDxehYfbLKME3KaxC02R2cE4",
    authDomain: "tucoin.firebaseapp.com",
    databaseURL: "https://tucoin.firebaseio.com",
    projectId: "tucoin",
    storageBucket: "tucoin.appspot.com",
    messagingSenderId: "809282658286"
  };
firebase.initializeApp(config);

export const database = firebase.firestore();
const settings = { timestampsInSnapshots: true };
firebase.firestore().settings(settings);
export const auth = firebase.auth();
export const storage = firebase.storage();