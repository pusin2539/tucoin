import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import App from './App';
import About from './components/About';
import NotFound from './components/NotFound';
import RegisterPage from './pages/RegisterPage';
import GetCashPage from './pages/GetCashPage';
import BuyCoinPage from './pages/BuyCoinPage';
import ViewHistoryPage from './pages/ViewHistoryPage';
import RegisterStudent from './pages/RegisterStudent';
import RegisterGuest from './pages/RegisterGuest';
import RegisterVendor from './pages/RegisterVendor';

class Routes extends Component {
    render() {
      return (
        <div className="App container">
        <Route exact path="/" component={App} />
        <Route exact path="/about" component={About} />
        <Route exact path="/notFound" component={NotFound} />
        <Route exact path="/RegisterPage" component={RegisterPage}/>
        <Route exact path="/GetCashPage" component={GetCashPage} />
        <Route exact path="/BuyCoinPage" component={BuyCoinPage} />
        <Route exact path="/ViewHistoryPage" component={ViewHistoryPage}/>
        <Route excat path="/RegisterStudent" component={RegisterStudent}/>
        <Route exact path="/RegisterGuest" component={RegisterGuest}/>
        <Route exact path="/RegisterVendor" component={RegisterVendor}/>
        </div>
      )
    }
  }
  
  export default Routes 