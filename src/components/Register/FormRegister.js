import React, { Component } from 'react'
import { Button, Checkbox, Form, Input, Radio, Select, TextArea,
  Responsive, Segment  } from 'semantic-ui-react'
import { auth, database } from "../../firebase";
import {
  DateInput,
  TimeInput,
  DateTimeInput,
  DatesRangeInput,
  Label
} from 'semantic-ui-calendar-react';
const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
]

class FormRegister extends Component {
  constructor(props){
    super(props)
    this.state={
      username:'',
      password:'',
      password2:'',
      firstname:'',
      lastname:'',
      birthdate:'',
      gender:'',
      type:'',
      email:'',
      phoneNumber:'',
      shopName:'',
      headLine:''
    }
    this.registeration = this.registeration.bind(this)
  }

registeration(){
  if(this.state.password === this.state.password2 && this.state.password!==''){
    if(this.state.type !== 'vendor'){
      auth.onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
      const docRef0 = database.collection("user").doc(this.state.username);
        docRef0
          .set({
            username:this.state.username,
            password:this.state.password,
            firstname:this.state.firstname,
            lastname:this.state.lastname,
            birthdate:this.state.birthdate,
            gender:this.state.gender,
            type:this.state.type,
            email:this.state.email,
            phoneNumber:this.state.phoneNumber,
            email: this.state.email,
            timestamp: new Date(),
            registerBy:firebaseUser.email
          })
          .then(()=>{
            alert("สมัครสมาชิกเสร็จสิ้น")
          })
          .catch(function(error) {
            alert("error user "+error);
          });
      
      const docRef1 = database.collection("userCoin").doc(this.state.username);
        docRef1
          .set({
            username:this.state.username,
            coin:100,
            timestamp: new Date(),
            registerBy:firebaseUser.email
        })
        .then(()=>{
            alert("เงินในบัญชี : 100")
          })
          .catch(function(error) {
            alert("error coin "+error);
          });
          
        }})
    }else{
      auth.onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
      const docRef0 = database.collection("user").doc(this.state.username);
        docRef0
          .set({
            username:this.state.username,
            password:this.state.password,
            firstname:this.state.firstname,
            lastname:this.state.lastname,
            birthdate:this.state.birthdate,
            gender:this.state.gender,
            type:this.state.type,
            email:this.state.email,
            phoneNumber:this.state.phoneNumber,
            shopName:this.state.shopName,
            email: this.state.email,
            timestamp: new Date(),
            registerBy:firebaseUser.email
          })
          .then(()=>{
            alert("สมัครสมาชิกเสร็จสิ้น "+this.state.username)
          })
          .catch(function(error) {
            alert(error+" user error");
          });
          const docRef1 = database.collection("userCoin").doc(this.state.username);
          docRef1
            .set({
              username:this.state.username,
              coin:0,
              timestamp: new Date(),
              registerBy:firebaseUser.email
          })
          
          .then(()=>{
            alert("เงินในบัญชี : 0")
          })
          .catch(function(error) {
            alert(error+" coin error");
          });
        }})
        

    }
  
  }else{
    alert("กรุณากรอกรหัสผ่านให้ถูกต้อง")
  }
}
  
handleChange = (e, { value }) => this.setState({ value })
handleUsernameChange =(e, { value })=>{
    this.setState({
      username:value
    })
}
componentWillUnmount(){
  this.setState({
      username:'',
      password:'',
      password2:'',
      firstname:'',
      lastname:'',
      birthdate:'',
      gender:'',
      type:'',
      email:'',
      phoneNumber:'',
      shopName:'',
      headLine:''
  })
}
componentDidMount(){
  this.setState({
    type:this.props.type
  })
  if(this.props.type == "student"){
    this.setState({
      headLine:"แบบฟอร์มนักศึกษา"
    })
  }else if(this.props.type == "guest"){
    this.setState({
      headLine:"แบบฟอร์มบุคคลทั่วไป"
    })
  }else{
    this.setState({
      headLine:"แบบฟอร์มผู้ขาย"
    })
  }
}
//onChange
onChangeUsername=(e, { value })=>{
  this.setState({
    username:value
  })
}
onChangePassword=(e, { value })=>{
  this.setState({
    password:value
  })
}
onChangePassword2=(e, { value })=>{
  this.setState({
    password2:value
  })
}
onChangeFirstname=(e, { value })=>{
  this.setState({
    firstname:value
  })
}
onChangeLastname=(e, { value })=>{
  this.setState({
    lastname:value
  })
}
onChangeBirthdate=(e, { value })=>{
  this.setState({
    birthdate:value
  })
}
onChangeGender=(e, { value })=>{
  this.setState({
    gender:value
  })
}
onChangeType=(e, { value })=>{
  this.setState({
    type:value
  })
}
onChangeEmail=(e, { value })=>{
  this.setState({
    email:value
  })
}
onChangePhoneNumber=(e, { value })=>{
  this.setState({
    phoneNumber:value
  })
}
onChangeShopName=(e, { value })=>{
  this.setState({
    shopName:value
  })
}

  render() {
    const { value } = this.state
    return (
      <Segment.Group >
      <Responsive as={Segment}>
        <h1 style={{marginTop:"3%",marginBottom:"3%",color:"#0000cc"}}>{this.state.headLine}</h1>
      <Form style={{marginLeft:"20%",marginRight:"20%",textAlign:"Left"}}
      onSubmit={this.registeration}> 
      <Form.Field control={Input} label='ชื่อบัญชีผู้ใช้' placeholder='Username' onChange={this.onChangeUsername}/>
          <Form.Field control={Input} type='password' 
          label='รหัสผ่าน' placeholder='password' 
          onChange={this.onChangePassword}/>
          <Form.Field control={Input} type='password' 
          label='ยืนยันรหัสผ่าน' placeholder='re-password' 
          onChange={this.onChangePassword2}/>
          <Form.Field control={Input} 
          label='ชื่อ' placeholder='First name'
          onChange={this.onChangeFirstname} />
          <Form.Field control={Input}
           label='นามสกุล' placeholder='Last name' 
           onChange={this.onChangeLastname}/>
          {/* <Form.Field control={Input} 
          label='Birth Date' placeholder='10/12/2560'
          onChange={this.onChangeBirthdate} /> */}
          <label style={{fontWeight:"bold",fontSize:"90%"}}>วัน/เดือน/ปี เกิด</label>
          <DateInput
          name="birthdate"
          placeholder="birthdate"
          value={this.state.birthdate}
          iconPosition="left"
          onChange={this.onChangeBirthdate}
        />

         <Form.Field control={Select} label='เพศ' 
         options={options} placeholder='Gender'
         onChange={this.onChangeGender} />
          <Form.Field control={Input} 
          label='ประเภทผู้ใช้'  value={this.state.type}
          />
          <Form.Field control={Input} 
          label='อีเมลล์' placeholder='example@tuexample.com'
          onChange={this.onChangeEmail} />
          <Form.Field control={Input} 
          label='หมายเลขโทรศัพท์' placeholder='0999999999'
          onChange={this.onChangePhoneNumber} />
        {this.state.type== "vendor"?(
          <Form.Group widths='equal'>
            <Form.Field control={Input}
             label='ชื่อร้านค้า' placeholder='Good food'
             onChange={this.onChangeShopName} />
          </Form.Group>
        ):(
          <p></p>
        )}
        {/* <Form.Field control={Checkbox} label='I agree to the Terms and Conditions' /> */}
        <Form.Field 
      style={{marginTop:"1%",marginLeft:'50%',marginRight:'50%'}} control={Button} color="green">Submit</Form.Field>
      </Form>
      

      </Responsive>
      </Segment.Group>
    )
  }
}

export default FormRegister