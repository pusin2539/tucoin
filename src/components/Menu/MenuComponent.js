import React, { Component } from 'react'
import { Input, Menu, Segment } from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import { Icon } from 'semantic-ui-react'
import {auth} from '../../firebase'

export default class MenuComponent extends Component {
  constructor(props){
    super(props)
    this.state ={
      activeItem: 'home' ,
      currentUser:null
    }
  }
  handleItemClick = (e, { name }) =>
   this.setState({ activeItem: name })

  componentDidMount(){
    auth.onAuthStateChanged(user => {
      if (user) {
        this.setState({
          currentUser: user
        })
      }else{
        this.setState({
          currentUser: null
        })
      }
    })
  }

  render() {
    const { activeItem } = this.state

    return (
      <div >
       
        <Menu pointing>
        <Menu.Item
              as={Link} 
              to='/RegisterPage' 
              name='สมัครสมาชิก'
              active={activeItem === 'สมัครสมาชิก'}
              onClick={this.handleItemClick}
              icon="edit outline"
            />
        
          <Menu.Item
              as={Link} 
              to='/BuyCoinPage' 
              name='เติมเงิน'
              active={activeItem === 'เติมเงิน'}
              onClick={this.handleItemClick}
              icon="arrow alternate circle down outline icon"
            />
          
          <Menu.Item
            name='ถอนเงิน'
            as={Link}
            to='/GetCashPage' 
            active={activeItem === 'ถอนเงิน'}
            onClick={this.handleItemClick}
            icon="arrow alternate circle up outline icon"

          />
          <Menu.Item
            name='ดูประวัติ'
            as={Link}
            to='/ViewHistoryPage' 
            active={activeItem === 'ดูประวัติ'}
            onClick={this.handleItemClick}
            icon="history"
          />
          <Menu.Menu position='right'>
          {this.state.currentUser!=null?(
            <p style={{marginTop:"5%",marginRight:"5%",color:"blue"}}>
              {this.state.currentUser.email}
            </p>
          ):(
            <p></p>
            
          )}
          {this.state.currentUser!==null?(
             <Menu.Item 
             icon="home"
             name='ออกจากระบบ' 
             as={Link} 
             to='/' 
             active={activeItem === 'ออกจากระบบ'} 
             onClick={this.handleItemClick} 
           />

          ):(
            <Menu.Item 
            icon="home"
            name='เข้าสู่ระบบ' 
            as={Link} 
            to='/' 
            active={activeItem === 'เข้าสู่ระบบ'} 
            onClick={this.handleItemClick} 
          />

          )}
         
           
          </Menu.Menu>
         
        </Menu>

       
      </div>
    )
  }
}