import React, { Component } from 'react'
import { Button, Checkbox, Form, Input, Radio, Select, TextArea,
  Responsive, Segment  } from 'semantic-ui-react'
import {auth} from '../../firebase'
import {Link} from "react-router-dom"

const options = [
  { key: 'm', text: 'Male', value: 'male' },
  { key: 'f', text: 'Female', value: 'female' },
]
class FormRegister extends Component {
  constructor(props){
    super(props)
    this.state={
      email: '',
      password: '',
      currentUser: null,
      message: ''
    }
  }
  
  componentDidMount(){
    auth.onAuthStateChanged(user => {
      if (user) {
        this.setState({
          currentUser: user
        })
      }
    })
  }
  logout = e => {
    e.preventDefault()
    auth.signOut().then(response => {
      this.setState({
        currentUser: null
      })
    })
  }

  onSubmit = e => {
    e.preventDefault()
    const { email, password } = this.state
    auth
      .signInWithEmailAndPassword(email, password)
      .then(response => {
        this.setState({
          currentUser: response.user
        })
      })
      .catch(error => {
        this.setState({
          message: error.message
        })
        alert(error)
      })
  }

  onChangeEmail =(e, { value })=>{
    this.setState({
      email:value
    })
}
onChangePassword=(e, { value })=>{
  this.setState({
    password:value
  })
}

  render() {
    const { value } = this.state
  
    return (
      <div>
      {this.state.currentUser !== null?(
         <div style={{marginTop:"10%",marginBottom:"20%",border:"1px solid black"
         ,marginLeft:"auto",marginRight:"auto",width:"60%"
         ,backgroundColor:"white",padding:"5%"}}>
         <h2 style={{color:"#0000cc"}}>ยินดีต้อนรับ</h2>
         <h3>{this.state.currentUser.email}</h3>
         <Button onClick={this.logout}
         style={{backgroundColor:"red",color:"white",marginTop:"3%"}}>
           ออกจากระบบ
         </Button>
         </div>
        
      ):(
        <Form onSubmit={this.onSubmit} 
        style={{boxShadow:"5px 5px 3px gray",marginLeft:"auto",
      marginRight:"auto",width:"60%",backgroundColor:"white",
      marginTop:"5%",marginBottom:"10%",border:"1px solid black"}} >
        <h1 style={{marginTop:"5%",marginBottom:"3%",color:"#0000cc"}}>Sign in</h1>
        <div style={{marginTop:"2%",paddingLeft:"20%",paddingRight:"20%"}}>
        <Form.Field iconPosition={"left"} icon={"user outline"} control={Input} label='Email' placeholder='Username' onChange={this.onChangeEmail}/>
        <Form.Field iconPosition={"left"} icon={"keyboard outline icon"} control={Input} type='password' label='Password' placeholder='Password'  onChange={this.onChangePassword}/>
        <Form.Field 
        style={{marginTop:"2%",marginBottom:"10%"}} 
        control={Button} 
        color="green"
        >Log in</Form.Field>
        </div> 
        </Form>

      )}
      
      {/* <img src={require('../../images/TUBanner.png')}/> */}
      <Link
      to={{ pathname: '/ViewHistoryPage', state: this.state.email }}
      >
      <Button>
      Go
      </Button>
      </Link>
      </div>
    )
  }
}

export default FormRegister