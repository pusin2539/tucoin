import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Responsive, Segment } from 'semantic-ui-react'
import MenuComponent from './components/Menu/MenuComponent'
import {Link} from 'react-router-dom'
import  LoginAdmin from './components/Login/LoginAdmin'

class App extends Component {
  constructor(props){
    super(props)
    
  }

  render() {
    return (
      <div className="App" >
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header> */}
        <Segment.Group >
          <Responsive as={Segment} style={{backgroundColor:"lightgray"}}>
            <MenuComponent/>
            <LoginAdmin />
          </Responsive>
        </Segment.Group>
        
      </div>
    );
  }
}

export default App;
