import React, { Component } from 'react'
import FormRegister from '../components/Register/FormRegister'
import MenuComponent from '../components/Menu/MenuComponent'
import { Button, Checkbox, Form, Input, Radio, Select, TextArea,
    Responsive, Segment, Grid,Image  } from 'semantic-ui-react'

    const options = [
        { key: 'm', text: 'Male', value: 'male' },
        { key: 'f', text: 'Female', value: 'female' },
      ]

export default class RegisterVendor extends Component{

    render(){
        return(
            <Segment.Group>
            <Responsive as={Segment}>
                <MenuComponent/>
                <FormRegister type={"vendor"}/>
            </Responsive>
            </Segment.Group>
        )
    }
}