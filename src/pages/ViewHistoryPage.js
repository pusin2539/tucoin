import React, { Component } from 'react'
import { Button, Checkbox, Form, Input, Radio, Select, TextArea,
  Responsive, Segment, Icon, Table  } from 'semantic-ui-react'
import MenuComponent from '../components/Menu/MenuComponent'
import { auth, database } from "../firebase";


class ViewHistoryPage extends Component {
  constructor(props){
    super(props)
    this.state={
      username:"",
      dataBuy:[],
      dataGet:[],
      checkType:'',
      userUsername:''

    }
    this.getType = this.getType.bind(this)
    this.handleUsernameChange = this.handleUsernameChange.bind(this)
    this.showBuyHistory = this.showBuyHistory.bind(this)
    this.showGetHistory = this.showGetHistory.bind(this)
  }
  


  handleUsernameChange =(e, { value })=>{
      this.setState({
        username:value,
      })
  }

  getType(){
    this.setState({
      dataBuy:[],
      dataGet:[],
      checkType:'',
      userUsername:''
    })
    if(this.state.username!=''){
    auth.onAuthStateChanged(firebaseUser => {
      if (firebaseUser) {
        const docRef = database.collection("user").doc(this.state.username);
        docRef.get().then(doc => {
          if (doc.id !== "") {
            this.setState({
              checkType : doc.data().type,
              userUsername:doc.data().username
            });
          }
        })
        .then(()=>{
          if(this.state.checkType == 'student'|| this.state.checkType == 'guest'){
            this.showBuyHistory()
          }else{
            this.showGetHistory()
          }
        })
        .catch(function(error) {
          alert("ไม่พบสมาชิกในระบบ");
        });;
      } 
    });
    }else{
      alert("กรุณากรอกชื่อบัญชีผู้ใช้")
    }
  }

    showBuyHistory() {
      const data = [];
      let i = 0;
      auth.onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
          const docRef = database
            .collection("buyCoinHistory")
            .where("username", "==", this.state.username);
          docRef.get().then(snapshot => {
            snapshot.docs.forEach(doc => {
              data[i] = {
                amount: doc.data().amount,
                transactionBy: doc.data().transactionBy,
                timestamp: doc.data().timestamp,
                
              };
              i++;
              if (i === snapshot.size) {
                this.setState({
                  dataBuy: data
                });
              }
            });
          });
        }
      });
    }
    showGetHistory() {
      const data = [];
      let i = 0;
      auth.onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
          const docRef = database
            .collection("getCashHistory")
            .where("username", "==", this.state.username);
          docRef.get().then(snapshot => {
            snapshot.docs.forEach(doc => {
              data[i] = {
                amount: doc.data().amount,
                transactionBy: doc.data().transactionBy,
                timestamp: doc.data().timestamp,
                
              };
              i++;
              if (i === snapshot.size) {
                this.setState({
                  dataGet: data
                });
              }
            });
          });
        }
      });
    }



  render() {
    const { value } = this.state

    return (
      <Segment.Group>
      <Responsive as={Segment} style={{backgroundColor:"lightgray"}}>
      <MenuComponent/>
      <Form style={{boxShadow:"5px 5px 3px gray",marginLeft:"auto",
      marginRight:"auto",width:"60%",backgroundColor:"white",
      padding:"3%",
      marginTop:"5%",border:"1px solid black"}}> 
          <h1 style={{fontFamily:"Impact",marginTop:"5%",marginBottom:"3%",color:"#0000cc"}}>ดูประวัติ</h1>
            <div style={{marginTop:"2%",paddingLeft:"20%",paddingRight:"20%"}}>
            <Form.Field iconPosition={"left"} icon={"user outline"} control={Input} label='Username' placeholder='Username' onChange={this.handleUsernameChange}/>
            <Form.Field 
            onClick={this.getType}
            style={{marginTop:"2%",marginBottom:"7%"}} control={Button} color="green">Submit</Form.Field>
            </div>
            <p>From : {this.props.history.location.state}</p>
      </Form>
      <div style={{width:"60%",marginLeft:"auto",marginRight:"auto" ,marginBottom:"10%",marginTop:"3%"}}>
      <Table celled striped>
        <Table.Header>
          {this.state.userUsername!=''?(
             <Table.Row>
             <Table.HeaderCell colSpan='3'>{this.state.userUsername} </Table.HeaderCell>
           </Table.Row>
          ):(
            console.log("x")
          )}
       
      </Table.Header>
    
    <Table.Body>
      {this.state.dataBuy.length >0?(
        this.state.dataBuy.map((doc)=>{
          return(
        <Table.Row>
        <Table.Cell collapsing>
          <Icon name='bitcoin' /> Buy Coin
        </Table.Cell>
        <Table.Cell>Amount : {doc.amount} , Transaction By : {doc.transactionBy}</Table.Cell>
        <Table.Cell collapsing textAlign='right'>
        {doc.timestamp
            .toDate()
            .getDate()
            .toString()}
            /
        {(doc.timestamp.toDate().getMonth() + 1).toString()}
            /
        {doc.timestamp
            .toDate()
            .getFullYear()
             .toString()}
        </Table.Cell>
      </Table.Row>
          )
        })
        
      ):(
          console.log("noting")
      )}

      {this.state.dataGet.length>0?(
         this.state.dataGet.map((doc)=>{
           return(
          <Table.Row>
        <Table.Cell collapsing>
          <Icon name='bitcoin' /> Get Cash
        </Table.Cell>
        <Table.Cell>Amount : {doc.amount} , Transaction By : {doc.transactionBy}</Table.Cell>
        <Table.Cell collapsing textAlign='right'>
        {doc.timestamp
            .toDate()
            .getDate()
            .toString()}
            /
        {(doc.timestamp.toDate().getMonth() + 1).toString()}
            /
        {doc.timestamp
            .toDate()
            .getFullYear()
             .toString()}
        </Table.Cell>
      </Table.Row>
           )
        })
      ):(
        console.log("noting")
      )}
   </Table.Body>     
    </Table>
    {this.state.dataBuy.length==0 &&this.state.dataGet.length==0&&this.state.userUsername!=''?(
      <h2>ไม่พบประวัติของ {this.state.userUsername}</h2>
    ):(
      console.log("no")
    )}
    </div>
      {/* <img src={require('../images/TUBanner.png')}/> */}
      </Responsive>
      </Segment.Group>
    )
  }
}

export default ViewHistoryPage