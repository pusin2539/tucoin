import React, { Component } from 'react'
import { Button, Checkbox, Form, Input, Radio, Select, TextArea,
  Responsive, Segment  } from 'semantic-ui-react'
import MenuComponent from '../components/Menu/MenuComponent'
import { auth, database } from "../firebase";

class GetCashPage extends Component {
  constructor(props){
    super(props)
    this.state={
      username:"",
      amount:0,
      buyCoin:false,
      userCoin:0,
      checkType:'',
      timestamp:'',
      userUsername:''
    }
    this.buyCoin = this.buyCoin.bind(this)
    this.canGet = this.canGet.bind(this)
    this.transactionSubmit = this.transactionSubmit.bind(this)
  }
  


  handleChange = (e, { value }) => this.setState({ value })
  handleUsernameChange =(e, { value })=>{
      this.setState({
        username:value,
        status:false
      })
  }
  handleAmoutChange =(e, { value })=>{
    this.setState({
      amount:parseInt(value),
      status:false
    })
    }
    buyCoin = (userStatus)=>{
        this.setState({
            status:userStatus
        })
    }
    getCoin(){
      auth.onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
          const docRef = database.collection("userCoin").doc(this.state.username);
          docRef.get().then(doc => {
            if (doc.id !== "") {
              this.setState({
                userCoin : doc.data().coin,
                timestamp: doc.data().timestamp,
                userUsername : doc.data().username
              });
            }
          })
          .then(()=>{
            this.getCash()
          });
        } 
      });
    }

    getCash(){
        if(this.state.username !== "" && this.state.amount >0){
          if(this.state.userCoin>=this.state.amount){
            auth.onAuthStateChanged(firebaseUser => {
              if (firebaseUser) {
            const docRef0 = database.collection("getCashHistory").doc();
              docRef0
                .set({
                  username:this.state.username,
                  amount:this.state.amount,
                  timestamp: new Date(),
                  transactionBy: firebaseUser.email
                })
                .catch(function(error) {
                  alert("history error");
                });
            const docRef1 = database.collection("userCoin").doc(this.state.username);
                  docRef1
                    .set({
                      coin:this.state.userCoin - this.state.amount,
                      username:this.state.userUsername,
                      timestamp:this.state.timestamp
                    })
           
                .then(()=>{
                  alert("เปลี่ยนเป็นเงินสดสำเร็จ"+"\n"+this.state.amount+" from "+this.state.username)
                })
                
                .catch(function(error) {
                  alert("getCash "+error);
                });
              }})               
            }else{
              alert("คุณมีเงินในระบบไม่พอ")
            }
        }else{
          alert("กรุณากรอกข้อมูลให้ถูกต้อง")
        }
    }

    transactionSubmit(){
      
          // this.getCoin()
          this.canGet()
      
        
      
    }

    canGet(){
      auth.onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
          const docRef = database.collection("user").doc(this.state.username);
          docRef.get().then(doc => {
            if (doc.id !== "") {
              this.setState({
                checkType : doc.data().type
              });
            }
          })
          .then(()=>{
            if(this.state.checkType == 'vendor'){
              this.getCoin()
            }else{
              alert("ขึ้นเงินได้เฉพาะร้านค้า")
            }
          })
          .catch(function(error) {
            alert(error);
          });;
        } 
      });
    }


  render() {
    const { value } = this.state
    return (
      <Segment.Group>
      <Responsive as={Segment} style={{backgroundColor:"lightgray"}}>
      <MenuComponent/>
      
      <Form style={{boxShadow:"5px 5px 3px gray",marginLeft:"auto",
      marginRight:"auto",width:"60%",backgroundColor:"white",
      marginTop:"5%",marginBottom:"15%",border:"1px solid black"}}> 
             <h1 style={{fontFamily:"Impact",marginTop:"5%",marginBottom:"3%",color:"#0000cc"}}>ถอนเงิน</h1>
            <div style={{marginTop:"2%",paddingLeft:"20%",paddingRight:"20%"}}>
            <Form.Field iconPosition={"left"} icon={"user outline"} control={Input} label='Username' placeholder='Username' onChange={this.handleUsernameChange}/>
            <Form.Field iconPosition={"left"} icon={"dollar sign"} control={Input} label='Amount' placeholder='ex. 1000' onChange={this.handleAmoutChange} />
            <Form.Field 
            onClick={()=>{this.transactionSubmit()}}
            style={{marginTop:"2%",marginBottom:"7%"}} control={Button} color="green">Submit</Form.Field>
            </div>
           
      </Form>
      
      
      {/* <img src={require('../images/TUBanner.png')}/> */}
      </Responsive>
      </Segment.Group>
    )
  }
}

export default GetCashPage