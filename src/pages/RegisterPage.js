import React, { Component } from 'react'
import MenuComponent from '../components/Menu/MenuComponent'
import { Responsive, Segment } from 'semantic-ui-react'
import { Grid,Image } from 'semantic-ui-react'
import {Link} from 'react-router-dom'

export default class RegisterPage extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
           
            <Segment.Group>
            <Responsive as={Segment} >
              <MenuComponent/>
              <h1 style={{marginTop:"3%",marginBottom:"3%",color:"#0000cc"}}>เลือกประเภทสมาชิก</h1>
              {/* <h2 style={{marginBottom:"3%"}}>นักศึกษา, บุคคลทั่วไป หรือผู้ขาย</h2> */}
            
              <Grid.Column key={2}>
              <Image src={require('../images/student.png')} 
              label="นักศึกษา"
              style={{height:"330px",border:"1px solid black",boxShadow:"5px 5px 3px gray"}}
              size='medium'
              as={Link} 
                to='/RegisterStudent' 
               />
               <Image src={require('../images/guest.jpg')} 
              label="บุคคลทั่วไป"
              style={{height:"330px",border:"1px solid black",boxShadow:"5px 5px 3px gray"}}
               size='medium'
              as={Link} 
                to='/RegisterGuest' 
               />
               <Image src={require('../images/vendor.jpg')} 
                label="ร้านค้า"
               style={{height:"330px",border:"1px solid black",boxShadow:"5px 5px 3px gray"}}
               size='medium'
              as={Link} 
                to='/RegisterVendor' 
               />
             </Grid.Column>
             <div style={{marginBottom:"10%"}}>

             </div>
            </Responsive>
          </Segment.Group>
        )
    }
}